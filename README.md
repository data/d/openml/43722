# OpenML dataset: Gen-1-Pokemon

https://www.openml.org/d/43722

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Dataset of the original 151 Pokemon with stats only from Generation 1. Data was scraped from https://serebii.net/. 
Content

Number: Pokedex Index Number
Name: Name of the Pokemon
Types: The number of types a Pokemon has
Type1: The primary type
Type2: The secondary type if applicable
Height: The height of the Pokemon in meters
Weight: The weight of the Pokemon in kilograms
Male_Pct: The probability of encountering a male in percentage
Female_Pct: The probability of encountering a female in percentage
Capt_Rate: Measures how difficult it is too capture the Pokemon (Higher value = Higher difficulty
Exp_Points: Experience points needed to fully level up
Exp_Speed: How fast a Pokemon fully levels up
Base_Total: The sum all the base stats (HP, Attack, Defense, Special, Speed)
HP - Speed: The base stats of a Pokemon
NormalDmg - DragonDmg: The multiplicative damage it takes from certain types
Evolutions: The number of evolutions the base Pokemon has

Acknowledgements
Scraped from https://serebii.net/.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43722) of an [OpenML dataset](https://www.openml.org/d/43722). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43722/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43722/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43722/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

